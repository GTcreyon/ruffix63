#!/usr/bin/env python
"""Build ruffix63 from an input SWF file by applying patches."""

import subprocess
from os import environ
from argparse import ArgumentParser
from pathlib import Path

parser = ArgumentParser(
    description="Build ruffix63 from an input SWF file by applying patches.",
)

parser.add_argument(
    "-i",
    "--input",
    type=Path,
    help="Input SWF file",
    default="2012.swf",
)
parser.add_argument(
    "-g",
    "--gameversion",
    type=str,
    required=False,
    help="Version of the game to be used, e.g. 2009, 2012",
    default="2012",
)
parser.add_argument(
    "-o",
    "--output",
    type=Path,
    required=False,
    help="Output path for the patched SWF",
    default="ruffix63.swf",
)
args = parser.parse_args()

filename = args.output

env = environ.copy()
env["63VER"] = args.gameversion

subprocess.run(
    [
        "flash-patcher",
        "--inputswf",
        args.input,
        "--folder",
        "patches",
        "--stagefile",
        f"{args.gameversion}.stage",
        "--outputswf",
        filename,
    ],
    check=True,
    env=env,
)
