# ruffix63
## Overview
ruffix63 is a set of patches for Super Mario 63 that apply workarounds to bring behavior on the Ruffle emulator up to par with the stock Flash Player.

## Changes
- Fix behavior of stacked Level Designer layers to only click one at a time

## Build
### Requirements
- [Riley's SWF Patcher](https://github.com/rayyaw/flash-patcher)
	- The `flash-patcher` utility must be accessible via the PATH environment variable.
	- opti63 has been designed and tested for compatibility with v5.3.0 of the Flash Patcher.
- [FFDec](https://github.com/jindrapetrik/jpexs-decompiler)
	- opti63 has been designed and tested for compatibility with v20.0.0 of FFDec.
- [Python](https://www.python.org/downloads)
- A clean copy of Super Mario 63, as a SWF file.
	- Either the 2009 or 2012 version will work, but take note of which you are using.
	- An executable bundle (.exe) will not work. You should either find a raw SWF version, or extract the SWF using an external tool such as [swf-unbundler](https://github.com/GTcreyon/swf-unbundler).
- [The contents of this repository.](https://gitlab.com/GTcreyon/ruffix63/-/archive/main/ruffix63-main.zip)

### Method
Run the command:
```bash
./build.py -i <PATH-TO-YOUR-SM63-SWF>.swf -g <VERSION>
```
- \<PATH-TO-YOUR-SM63-SWF> is the path to the clean Super Mario 63 SWF that you intend to patch
- \<VERSION> is the identifier for the version of the game that the SWF contains. This will be either 2009 or 2012.

Optionally, you may specify an output file path using the `-o`/`--output` option.

For example...
```bash
./build.py -i 2012.swf -g 2012 -o ruffix2012.swf
```
...will use the SWF file `2012.swf` in the current directory, applying the appropriate patches for the 2012 version, and save the result in `ruffix2012.swf`, also in the current directory.